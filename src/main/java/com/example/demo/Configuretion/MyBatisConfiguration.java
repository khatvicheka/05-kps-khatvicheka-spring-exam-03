package com.example.demo.Configuretion;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

public class MyBatisConfiguration {
    private DataSource dataSource;
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.dataSource=dataSource;
    }
    @Bean
    public DataSourceTransactionManager managerDataSource(){
        return new DataSourceTransactionManager(dataSource);
    }
    @Bean
    SqlSessionFactoryBean factoryBean(){
        SqlSessionFactoryBean bean=new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        return bean;
    }
}
