package com.example.demo.Configuretion;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

public class jdbcConfiguretion {
    @Bean
    public DataSource myPostgresDb(){
        DataSourceBuilder dataSource = DataSourceBuilder.create();
        dataSource.driverClassName("org.postgresql.Driver");
        dataSource.url("jdbc:postgresql://localhost:5432/article_db");
        dataSource.username("postgres");
        dataSource.password("postgres001");
        return dataSource.build();
    }
}
